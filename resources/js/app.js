require('./bootstrap');
import moment from 'moment';
import VueRouter from 'vue-router';
import Index from './Index';
import router from './routes';
import Vuex from 'vuex';

import StarRating from './shared/components/StarRating';
import FatalError from './shared/components/FatalError';
import Success from './shared/components/Success';
import ValidationErrors from './shared/components/ValidationErrors';

import storeDefinition from './store';

window.Vue = require('vue');


// Vue router
Vue.use(VueRouter);

Vue.filter("fromNow", value => moment(value).fromNow());

Vue.component("star-rating", StarRating);
Vue.component("fatal-error", FatalError);
Vue.component("success", Success);
Vue.component("v-errors", ValidationErrors);

// Vuex
Vue.use(Vuex)

const store = new Vuex.Store(storeDefinition);

window.axios.interceptors.response.use(
    response => {
        return response;  
    },
    error => {
        console.log(error.response);
        if(401 == error.response.status){
            store.dispatch("logout");
        }

        return Promise.reject(error);
    }
)

const app = new Vue({
    el: '#app',
    router,
    store,
    components: {
        index: Index
    },
    async beforeCreate() {
        this.$store.dispatch('loadStoredState');
        this.$store.dispatch('loadUser');
    }
});